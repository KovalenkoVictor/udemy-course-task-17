import 'package:flutter/material.dart';
import 'package:udemy_course_task17/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Udemy course task 17',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool createList = true;
  bool reverse = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Container(
            height: 600,
            margin: EdgeInsets.fromLTRB(20.0, 24.0, 20.0, 20.0),
            color: Colors.white12,
            child: ListView.builder(
              reverse:reverse,
              itemBuilder: (context, index) {
                return Center(
                  child: Container(
                    width: double.infinity,
                    height: 150.0,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      border: Border.all(width: 5.0),
                    ),
                    child: Text('${MyProvider.list[index]}'),
                  ),
                );
              },
              itemCount: createList ? 0 : MyProvider.list.length,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      createList =! createList;
                    });
                  },
                  child: Text('Generate list')),
              ElevatedButton(onPressed: () {
                setState(() {
                  reverse=!reverse;
                });
              }, child: Text('Sort')),
              ElevatedButton(
                  onPressed: () {
                   setState(() {
                     MyProvider.removeRandomListItem();
                   });
                  },
                  child: Text('Remove random item'))
            ],
          )
        ],
      ),
    );
  }
}
